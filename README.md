# Atelier d'introduction à la science des données

Matériel d'une courte formation d'initiation à la science des données développée à l'origine pour des élèves de 4e secondaire.

## Auteur

Vincent Goulet, professeur titulaire, École d'actuariat, Université Laval

## Résumé

La science des données est l'extraction de connaissance d'ensembles de données. C'est une discipline qui s'appuie sur des outils de mathématiques, de statistique, d'informatique, et de visualisation des données.

Cet atelier propose une initiation à la science des données basée sur un exemple classique d'apprentissage automatique ou, comme on dit aujourd'hui, d'intelligence articielle: la reconnaissance optique de caractères.

Les participants à la formation auront l'occasion de toucher à toutes les principales étapes de modélisation en intelligence artificielle: utilisation d'une grande quantité de données d'entrainement; choix d'une règle de décision; application à des nouvelles données; prévision.

La formation utilise [R](https://r-project.org) pour l'analyse et la visualisation des données, ainsi que les paquetages additionnels [RSKC](https://cran.r-project.org/package=RSKC) et [magick](https://cran.r-project.org/package=magick). Aucune connaissance préalable de R n'est nécessaire.
 
## Durée

90 minutes
